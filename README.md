Na základe skúseností a znalostí, pri tvorbe C# aplikácíi a skutočnosti, že nemám reálne skúsenosti pri tvorbe web app
sam sa pre demonštratívne účely rozhodol využiť consol aplication

Vytvoril som databázu za použitia singleton patternu a utilities triedu spravujúcu jej operácie

Samotná aplikácia simuluje prijímanie príkazu z api a nahradzuje ho príkazovým riadkom.
Run class reprezentuje runtime logic aplikácie a na jej vytvorenie bol použitý fascade pattern.

Ďalej by bolo možné použiť napríklad Factory pattern na tvorbu viacerých typov užíateľov.

Samotná tvorba aplikácie spolu s testovaním zabrala približne 5 hodín. Určitý čas som venoval aj zozbieraniu informácií o design patternoch a vybraniu vhodných patternov pre aplikáciu.
