﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAT
{
    public class Status
    {
        public int Id { get; set; } 
        public User Author { get; set; }
        public List<Commentary> Commentaries { get; set; }

        public string Text { get; set; }
    }
}
