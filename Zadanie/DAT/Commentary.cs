﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAT
{
    public class Commentary
    {
        public int Id { get; set; }
        public User Author { get; set; }

        public Status Status { get; set; }
        public List<Commentary> Responses { get; set; }

        public string Text { get; set; }
    }
}
