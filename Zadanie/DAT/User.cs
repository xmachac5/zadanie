﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

/**
 * Possible use of factory in creating more types of users
 **/

namespace DAT
{
    public class User
    {
        [Key]
        public string Name { get; set; }

        public List<Message> Messages { get; set; }

        public List<Status> Statuses { get; set; }

        public List<Commentary> Commentaries { get; set; }

        public string Password { get; set; }
    }
}
