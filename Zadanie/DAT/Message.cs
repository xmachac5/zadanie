﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAT
{
    public class Message
    {
        public int Id { get; set; }
        public User Author { get; set; }
        public User TargetUser { get; set; }

        public List<Text> MessageText { get; set; }
    }
}
