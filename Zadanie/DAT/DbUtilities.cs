﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DAT
{
    public static class DbUtilities
    {
        private static ForumDbContext dbContext = ForumDbContext.GetInstance();

        public static User AddUser(string name, string password)
        {
            if (dbContext.Users
                .Where(user => user.Name == name)
                .Count() > 0)
            {
                Console.WriteLine(name + " user already exists\n");
                return null;
            }
            User user = new User
            {
                Name = name,
                Password = password,
                Messages = new List<Message>(),
                Statuses = new List<Status>(),
                Commentaries = new List<Commentary>(),
            };
            dbContext.Users.Add(user);
            dbContext.SaveChanges();
            return user;
        }

        public static User Login(string userName, string password)
        {
            var user = dbContext.Users
                .Where(user => user.Name == userName);
            if (user.Count() == 0)
            {
                Console.WriteLine("No user with given name");
                return null;
            }
            if (user.First().Password.Equals(password))
            {
                return user.First();
            }
            Console.WriteLine("Wrong password");
            return null;
        }

        public static void ShowUsers()
        {
            foreach (var name in dbContext.Users.Select(user => user.Name))
            {
                Console.WriteLine(name);
            }
        }

        public static void AddStatus(string text, User user)
        {
            dbContext.Statuses.Add(new Status
            {
                Author = user,
                Text = text
            });
            dbContext.SaveChanges();
        }

        public static void ShowStatuses(string userName)
        {
            foreach(var status in dbContext.Users
                .Where(user => user.Name == userName)
                .Select(user => user.Statuses)
                .First())
            {
                Console.WriteLine(status.Id + ": " + status.Text);
            }
        }

        public static void AddMesage(User author, string targetName, string text)
        {
            var target = dbContext.Users
                    .Where(user => user.Name == targetName);
            if (target.Count() == 0)
            {
                Console.WriteLine("Unexisting target user!\n");
                return;
            }
            var messages = dbContext.Messages
                .Where(message => message.Author == author && message.TargetUser == target.First());
            var messageText = new Text
            {
                TextText = text
            };
            if (messages.Count() == 0)
            {
                var message = new Message
                {
                    Author = author,
                    TargetUser = target.First(),
                    MessageText = new List<Text>()
                };
                message.MessageText.Add(messageText);
            }
            else
            {
                messages.First().MessageText.Add(messageText);
            }
            dbContext.SaveChanges();
        }

        public static void ShowMessages(User author, string targetName)
        {
            var target = dbContext.Users
                    .Where(user => user.Name == targetName);
            if (target.Count() == 0)
            {
                Console.WriteLine("Unexisting target user!\n");
                return;
            }
            foreach(var message in dbContext.Messages
                .Where(message => (message.TargetUser == target.First() && message.Author == author) ||
                (message.TargetUser == author && message.Author == target.First())))
            {
                if (message != null && message.MessageText != null)
                {
                    foreach (var tex in message.MessageText)
                    {
                            Console.WriteLine(tex.TextText);
                    }
                }
            }
        }
    }
    /** Commentaries will be done similary to mesages and statuses,
     * user can create commentary on status and commentary on commentary
     * user can also show all commentaries on staus or commentary
     **/
}
