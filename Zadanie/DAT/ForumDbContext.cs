﻿
/**
 * Databse
 **/

using Microsoft.EntityFrameworkCore;

namespace DAT
{
    public class ForumDbContext : DbContext
    {
        private string connectionString =
            @"server=(localdb)\MSSQLLocalDB; 
            Initial Catalog=OrderSystemDB; Integrated Security=true";

        public DbSet<User> Users { get; set; }

        public DbSet<Status> Statuses { get; set; }

        public DbSet<Commentary> Commentaries { get; set; }

        public DbSet<Message> Messages { get; set; }


        private ForumDbContext() : base()
        {
            Database.EnsureCreated();
        }

        private static ForumDbContext _instance;

        public static ForumDbContext GetInstance()
        {
            if (_instance == null)
            {
                _instance = new ForumDbContext();
            }
            return _instance;
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(connectionString);
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Message>()
                .HasOne(m => m.Author)
                .WithMany(a => a.Messages);
        }
    }
}
