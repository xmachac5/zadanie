﻿using DAT;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadanie
{
    public static class Register
    {
        public static User RegisterLogin(string action)
        {
            User user = null;
            string name;
            string password;
            int counter = 0;
            while (user == null && counter != 3)
            {
                Console.WriteLine("Enter name");
                name = Console.ReadLine();
                Console.WriteLine("Enter password");
                password = Console.ReadLine();
                if (action == "Register")
                {
                    user = DbUtilities.AddUser(name, password);
                }
                else
                {
                    user = DbUtilities.Login(name, password);
                }
                counter++;
            }
            return user;
        }
    }
}
