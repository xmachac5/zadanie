﻿/**
 * Done as console aplication for demostrative purpouse, because lack of skill in creating web app
 **/

namespace Zadanie
{
    class Program
    {
        static void Main(string[] args)
        {
            int status = 0;
            status = Run.AppBegin();
            while (status == 0)
            {
               if (Run.AppRun() == 0)
                {
                    status = Run.AppBegin();
                }
            }
        }
    }
}
