﻿using DAT;
using System;

/** 
 * Runtime logic of aplication
 **/

namespace Zadanie
{
    public static class Run
    {
        private static User actualUser = null;

        public static void Help()
        {
            Console.WriteLine("Suported operations:\n" +
                "Write message\n" +
                "Write status\n" +
                "Show users\n" +
                "Show messages\n" +
                "Show statuses\n" +
                "Logout");
        }

        public static int AppBegin()
        {
            while(actualUser == null)
            {
                Console.WriteLine("Type Login or Register if you are new. For exit type Exit.");
                string action = Console.ReadLine();
                {
                    switch (action)
                    {
                        case "Register":
                            actualUser = Register.RegisterLogin(action);
                            break;
                        case "Login":
                            actualUser = Register.RegisterLogin(action);
                            break;
                        case "Exit":
                            return 1;
                        default:
                            Console.WriteLine("Unsuported operation write Login, Register, or Exit");
                            break;
                    }
                }
            }
            return 0;
        }
        public static int AppRun()
        {
            if (actualUser == null)
            {
                Console.WriteLine("Not logged in.");
                if (AppBegin() == 1)
                {
                    return 1;
                }
            }
            while (true)
            {
                Help();
                string action = Console.ReadLine();
                switch (action)
                {
                    case "Logout":
                        actualUser = null;
                        return 0;
                    case "Write message":
                        Console.WriteLine("Insert target user name");
                        string targetUser = Console.ReadLine();
                        Console.WriteLine("Insert message text");
                        string messageText = Console.ReadLine();
                        DbUtilities.AddMesage(actualUser, targetUser, messageText);
                        break;
                    case "Write status":
                        Console.WriteLine("Insert status text");
                        string statusText = Console.ReadLine();
                        DbUtilities.AddStatus(statusText, actualUser);
                        break;
                    case "Show users":
                        DbUtilities.ShowUsers();
                        break;
                    case "Show messages":
                        Console.WriteLine("Insert target user name");
                        string user = Console.ReadLine();
                        DbUtilities.ShowMessages(actualUser, user);
                        break;
                    case "Show statuses":
                        DbUtilities.ShowStatuses(actualUser.Name);
                        break;
                    default:
                        Console.WriteLine("Unsuported operation.\n");
                        Help();
                        break;
                }
            }
        }
    }
}
